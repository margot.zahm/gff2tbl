# gff2tbl

## Usage

```
gff2tbl.py [-h] -g GFF -fa FASTA [-gene GENE_PRODUCT] [-lab LAB_NAME]
           [-o OUTPUT]

Converts a gff file to a tbl file.

optional arguments:
  -h, --help            show this help message and exit
  -g GFF, --gff GFF     path to a GFF file (default : stdin)
  -fa FASTA, --fasta FASTA
                        path to fasta file
  -gene GENE_PRODUCT, --gene_product GENE_PRODUCT
                        path to table of gene id and its corresponding product
  -lab LAB_NAME, --lab_name LAB_NAME
                        lab name to put in protein ID (default: 'lab')
  -o OUTPUT, --output OUTPUT
                        output file name (default : stdout)
```
