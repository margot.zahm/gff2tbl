import sqlite3

order_of_features = {
	'gene': 0,
	'mRNA': 1,
	'tRNA': 1,
	'exon': 2,
	'CDS': 3,
	'five_prime_UTR': 4,
	'three_prime_UTR': 4,
}


def extractInfo(info_column, element_to_find):
	"""Extract the sequence ID in GFF"""
	if element_to_find in info_column:
		info_list = info_column.split(";")

		for element in info_list:
			if element_to_find in element:
				element_found = element.split("=", 1)[1]
				break
		
		return element_found

	else:
		return ""


def sortGff(file):
	"""Sort Gff by coordinates, feature and chromosome"""
	extracted_lines = []

	for line in file:
		if line[0] != "#" and line: # Skeep comment line and empty lines
			line = line.rstrip().split('\t')

			if not line[2] in order_of_features:
				raise NameError(f"The feature {line[2]} is unknown.")
				
			extracted_lines.append(line)

	extracted_lines.sort(key = lambda x: int(x[3])) # Sort by coordinates
	extracted_lines.sort(key = lambda x: x[0]) # Sort by chromosome
	extracted_lines.sort(key = lambda x: order_of_features[x[2]]) # Sort by features following a fixed order

	return extracted_lines


class GffDB:
	def __init__(self, database="gff_database.db"):
		self.conn = sqlite3.connect(database)
		self.c    = self.conn.cursor()

	def createDatabase(self):

		self.execute('''DROP TABLE IF EXISTS CdsCoord''')
		self.execute('''DROP TABLE IF EXISTS Cds''')
		self.execute('''DROP TABLE IF EXISTS RnaCoord''')
		self.execute('''DROP TABLE IF EXISTS Rna''')
		self.execute('''DROP TABLE IF EXISTS GeneCoord''')
		self.execute('''DROP TABLE IF EXISTS Gene''')

		self.execute('''CREATE TABLE Gene
			(GeneID          INTEGER           PRIMARY KEY,
			 Chr             VARCHAR(50)       NOT NULL,
			 Start           BIGINT  UNSIGNED  NOT NULL,
			 Stop            BIGINT  UNSIGNED  NOT NULL,
			 Strand          SMALLINT          NOT NULL  CHECK (Strand == 1 OR Strand==-1),
			 Symbol          VARCHAR(20)       NOT NULL  DEFAULT  '',
			 Locus           VARCHAR(50)       NOT NULL  UNIQUE,
			 ThreeUtr        VARCHAR(1)        NOT NULL  DEFAULT  '',
			 FiveUtr         VARCHAR(1)        NOT NULL  DEFAULT  '',
			 Pseudo          VARCHAR(1)        NOT NULL  DEFAULT  '');
			''')

		self.execute('''CREATE TABLE Rna
			(RnaID           INTEGER           PRIMARY KEY,
			 GeneID          INTEGER           NOT NULL,
			 Locus           VARCHAR(50)       NOT NULL  UNIQUE,
			 Product         VARCHAR(300),
			 Feature         VARCHAR(50)       NOT NULL,
			 ThreeUtr        VARCHAR(1)        NOT NULL  DEFAULT  '',
			 FiveUtr         VARCHAR(1)        NOT NULL  DEFAULT  '',
			 Pseudo          VARCHAR(1)        NOT NULL  DEFAULT  '',
			 Dbref           VARCHAR(50),
			 Note            VARCHAR(500),
			 FOREIGN KEY (GeneID) REFERENCES Gene(GeneID));
			''')

		self.execute('''CREATE TABLE RnaCoord
			(PosID   INTEGER           PRIMARY KEY,
			 RnaID   INTEGER           NOT NULL,
			 Start   BIGINT  UNSIGNED  NOT NULL,
			 Stop    BIGINT  UNSIGNED  NOT NULL,
			 FOREIGN KEY (RnaID) REFERENCES Rna(RnaID));
			''')

		self.execute('''CREATE TABLE Cds
			(CdsID           INTEGER           PRIMARY KEY,
			 RnaID           INTEGER           NOT NULL,
			 Locus           VARCHAR(50)       NOT NULL  UNIQUE,
			 StartCodon      VARCHAR(1)        NOT NULL  DEFAULT  '',
			 StopCodon       VARCHAR(1)        NOT NULL  DEFAULT  '',
			 Pseudo          VARCHAR(1)        NOT NULL  DEFAULT  '',
			 FOREIGN KEY (RnaID) REFERENCES Rna(RnaID));
			''')

		self.execute('''CREATE TABLE CdsCoord
			(PosID   INTEGER           PRIMARY KEY,
			 CdsID   INTEGER           NOT NULL,
			 Start   BIGINT  UNSIGNED  NOT NULL,
			 Stop    BIGINT  UNSIGNED  NOT NULL,
			 FOREIGN KEY (CdsID) REFERENCES Cds(CdsID));
			''')

		self.conn.commit()

	def fetchAll(self, command):
		self.c.execute(command)
		return self.c.fetchall()

	def execute(self, command):
		try:
			self.c.execute(command)
		except sqlite3.OperationalError as e:
			print(f"Bad statement: {e}")
			return f"Bad statement: {command}"

	def extractId(self, table, gene_locus):
		self.c.execute(f"SELECT {table}ID FROM {table} WHERE Locus=?", (gene_locus,))

		try:
			gene_id = self.c.fetchall()[0][0]
			return gene_id
		except IndexError as e:
			return False


def loadGff(gff_file, database):
	gff_db = GffDB(database)
	gff_db.createDatabase()

	extracted_lines = sortGff(gff_file)

	for line in extracted_lines:
		start      = int(line[3])
		stop       = int(line[4])
		locus      = extractInfo(line[8], 'ID')

		if line[2] == "gene":
			chromosome = line[0]
			strand     = 1 if line[6] == '+' else -1
			symbol     = extractInfo(line[8], 'Symbol')

			gff_db.execute(f"INSERT INTO Gene (Chr, Start, Stop, Strand, Locus) VALUES ('{chromosome}', {start}, {stop}, {strand}, '{locus}')")
			gff_db.conn.commit()

		if 'RNA' in line[2]:
			parent  = extractInfo(line[8], 'Parent')
			gene_id = gff_db.extractId('Gene', parent)
			db_ref  = extractInfo(line[8], 'Dbxref')
			note    = extractInfo(line[8], 'Note').replace("'", "''")

			gff_db.execute(f"INSERT INTO Rna (GeneID, Locus, Feature, Dbref, Note) VALUES ({gene_id}, '{locus}', '{line[2]}', '{db_ref}', '{note}')")
			gff_db.conn.commit()

		if line[2] == 'exon':
			parents  = extractInfo(line[8], 'Parent')
			for parent in parents.split(","):
				rna_id = gff_db.extractId('Rna', parent)

				if rna_id:
					gff_db.execute(f"INSERT INTO RnaCoord (RnaID, Start, Stop) VALUES ({rna_id}, {start}, {stop})")
					gff_db.conn.commit()
				else:
					raise NameError(f"exon {locus} has no parent {parent}.")

		if line[2] == 'CDS':
			cds_id = gff_db.extractId('Cds', locus)

			if cds_id:
				gff_db.execute(f"INSERT INTO CdsCoord (CdsID, Start, Stop) VALUES ({cds_id}, {start}, {stop})")
				gff_db.conn.commit()
			else:
				parent = extractInfo(line[8], 'Parent')
				rna_id = gff_db.extractId('Rna', parent)

				if rna_id:
					gff_db.execute(f"INSERT INTO Cds (RnaID, Locus) VALUES ({rna_id}, '{locus}')")
					gff_db.conn.commit()

					cds_id = gff_db.extractId('Cds', locus)
					gff_db.execute(f"INSERT INTO CdsCoord (CdsID, Start, Stop) VALUES ({cds_id}, {start}, {stop})")
					gff_db.conn.commit()

				else:
					raise NameError(f"cds {locus} has no parent {parent}.")

	return gff_db


def insertProduct(product_file, gff_db):
	with open(product_file, 'r') as file:
		for line in file:
			line = line.rstrip().split('\t')

			locus, product = line
			
			gff_db.c.execute(f"UPDATE Rna SET Product='{product}' WHERE Locus='{locus}'")
			gff_db.conn.commit()