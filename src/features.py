def findN(gene_start, gene_stop, chr_seq):
	gene_seq = chr_seq[gene_start - 1:gene_stop].upper()
	before, after = (0,0)

	if gene_seq[0] == 'N':
		while gene_seq[before] == 'N':
			before += 1

	if gene_seq[-1].upper() =='N':
		after = 1
		while gene_seq[-after].upper() == 'N':
			after += 1
		after -= 1

	return before, after


def checkIntronLength(cds_coord):
	i=1 
	start = cds_coord[0][-1] + 1
	IntronCorrect = True

	while i < len(cds_coord): 
		stop = cds_coord[i][0] - 1 
		length = stop - start

		if length < 10:
			IntronCorrect = False
			break

		start = cds_coord[i][1] + 1
		i+=1 

	return IntronCorrect

def checkProt(cds_info, cds_coord, chr_seq, strand):
	correctIntron = checkIntronLength(cds_coord)

	if correctIntron:
		cds_seq = ''

		for coord in cds_coord:
			seq = chr_seq.seq[coord[0]-1:coord[1]]
			cds_seq += seq

		prot_seq = cds_seq.translate() if strand == 1 else cds_seq.reverse_complement().translate()

		if prot_seq[0] != 'M':
			cds_info[3] = '<'
			print(f"CDS {cds_info[2]} does not have start codon")
		
		if prot_seq.find('*') == -1:
			cds_info[4] = '>'
			print(f"CDS {cds_info[2]} does not have stop codon")
		elif prot_seq.find('*') + 1 != len(prot_seq):
			cds_info[5] = '\t' * 3 + 'pseudo\n'
			print(f"CDS {cds_info[2]} has internal stop codon")

		cds_info.append(cds_coord)
		return cds_info[2:]
	else:
		print(f"CDS {cds_info[2]} removed due to too short intron (<10bp).")
		return []


def correctFeatures(gene, gff_db, chr_seq):
	before, after = findN(gene[2], gene[3], chr_seq)

	if before:
		gene[2]  += before
		print(f"Gene {gene[6]} begins in gap: coord changed.")
	if after:
		gene[3] -= after
		print(f"Gene {gene[6]} ends in gap: coord changed.")

	rna_list  = [list(rna) for rna in gff_db.fetchAll(f"SELECT * FROM Rna WHERE GeneID = {gene[0]}")]
	cds_list  = []

	for rna in rna_list:
		feature   = rna[4]
		rna_coord = [list(coord)[2:] for coord in gff_db.fetchAll(f"SELECT * FROM RnaCoord WHERE RnaID = {rna[0]}")]

		if before and rna_coord[0][0] < gene[2]:
			rna_coord[0][0] = gene[2]
		if after and rna_coord[-1][-1] > gene[3]:
			rna_coord[-1][-1] = gene[3]

		rna.append(rna_coord)
		cds_info  = gff_db.fetchAll(f"SELECT * FROM Cds WHERE RnaID = {rna[0]}")

		if cds_info and feature == 'mRNA':
			cds_info  = list(cds_info[0])
			cds_coord = [list(coord)[2:] for coord in gff_db.fetchAll(f"SELECT * FROM CdsCoord WHERE CdsID = {cds_info[0]}")]

			if before and cds_coord[0][0] < gene[2]:
				cds_coord[0][0] = gene[2]
			if after and cds_coord[-1][-1] > gene[3]:
				cds_coord[-1][-1] = gene[3]
			if cds_coord[0][0] == gene[2]:
				gene[7] = '<'
				rna[5]  = '<'
			if cds_coord[-1][-1] == gene[3]:
				gene[8] = '>'
				rna[6]  = '>'

			cds_info = checkProt(cds_info, cds_coord, chr_seq, gene[4])

			if not cds_info or cds_info[3]:
				gene[9]  = '\t' * 3 + 'pseudo\n'
				rna[7] = '\t' * 3 + 'pseudo\n'

			cds_list.append(cds_info)

		elif feature == 'tRNA':
			trna_size = rna_coord[-1][-1] - rna_coord[0][0]
			if trna_size > 150:
				gene[9]  = '\t' * 3 + 'pseudo\n'
				rna[7] = '\t' * 3 + 'pseudo\n'

		del rna[0:2]

	return gene[2:], rna_list, cds_list


def sortCoord(coord, strand):
	reverse = True if strand == -1 else False
	coord = sorted([sorted(pos, reverse = reverse) for pos in coord], reverse = reverse)
	return coord


def generateTbl(gene, rna_list, cds_list, lab, output):
	strand     = gene[2]
	gene_start = gene[0] if strand == 1 else gene[1]
	gene_stop  = gene[1] if strand == 1 else gene[0]
	output.write(f"{gene[5]}{gene_start}\t{gene[6]}{gene_stop}\tgene\n")

	if gene[3]:
		output.write(f"\t\t\tgene\t{gene[3]}\n")

	output.write(f"\t\t\tlocus_tag\t{gene[4]}\n")
	output.write(gene[7])

	for index, rna in enumerate(rna_list):
		rna_coord = rna[-1]
		rna_coord = sortCoord(rna_coord, strand)

		feature   = rna[2]
		product   = rna[1] if rna[1] else "hypothetical protein"
		rna_coord[0].append(feature)

		if feature == 'mRNA':
			prot_id          = rna[0]
			rna_coord[0][0]  = rna[3] + str(rna_coord[0][0])
			rna_coord[-1][1] = rna[4] + str(rna_coord[-1][1])
			db_ref           = rna[6]
			note             = rna[7]

			for positions in rna_coord:
				positions  = list(map(str, positions))
				positions  = "\t".join(positions)
				output.write(f"{positions}\n")

			if not rna[5]:
				output.write(f"\t\t\tproduct\t{product}\n")

			output.write(f"\t\t\tprotein_id\tgnl|{lab}|{prot_id}\n")
			output.write(f"\t\t\ttranscript_id\tgnl|{lab}|mrna.{prot_id}\n")

			if note:
				output.write(f"\t\t\tnote\t{note}\n")

			cds               = cds_list[index]

			if cds:
				trans_id          = cds[0]
				pseudo            = cds[4]
				cds_coord         = sortCoord(cds[-1], strand)
				cds_coord[0][0]   = cds[1] + str(cds_coord[0][0])
				cds_coord[-1][1]  = cds[2] + str(cds_coord[-1][1])
				cds_coord[0].append('CDS')

				for positions in cds_coord:
					positions = list(map(str, positions))
					positions  = "\t".join(positions)
					output.write(f"{positions}\n")

				if not cds[3]:
					output.write(f"\t\t\tproduct\t{product}\n")
				output.write(cds[3])
				output.write(f"\t\t\tprotein_id\tgnl|{lab}|{prot_id}\n")
				output.write(f"\t\t\ttranscript_id\tgnl|{lab}|mrna.{prot_id}\n")

				if db_ref:
					[output.write(f"\t\t\tdb_xref\t{db}\n") for db in db_ref.split(',')]

		elif feature == 'tRNA':
			for positions in rna_coord:
				positions  = list(map(str, positions))
				positions  = "\t".join(positions)
				output.write(f"{positions}\n")

			product = rna[1] if rna[1] else 'tRNA-Xxx'
			output.write(f"\t\t\tproduct\t{product}\n")
			output.write(f"{rna[5]}")
