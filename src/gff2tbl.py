#!/usr/bin/env python

from features import correctFeatures, generateTbl
from dbgff import loadGff, insertProduct
from Bio import SeqIO
import argparse
import sys


def main(gff_file, output, fasta_file, gene_product_table, lab):
	fasta_dict = SeqIO.to_dict(SeqIO.parse(args.fasta, "fasta"))

	gff_db = loadGff(gff_file, 'gff.db')

	if gene_product_table:
		insertProduct(gene_product_table, gff_db)

	chromosome = ''
	gene_list  = [list(gene) for gene in gff_db.fetchAll("SELECT * FROM Gene ")]

	for gene in gene_list:
		if chromosome != gene[1]:
			chromosome = gene[1]
			chr_seq    = fasta_dict[chromosome]
			output.write(f">Features {chromosome}\n")

		gene, rna_list, cds_list = correctFeatures(gene, gff_db, chr_seq)
		generateTbl(gene, rna_list, cds_list, lab, output)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Converts a gff file to a tbl file.')
	parser.add_argument("-g", "--gff", type=str, required=True, help="path to a GFF file (default : stdin)")
	parser.add_argument("-fa", "--fasta", type=str, required=True, help="path to fasta file")
	parser.add_argument("-gene", "--gene_product", type=str, default = "", help="path to table of gene id and its corresponding product")
	parser.add_argument("-lab", "--lab_name", type=str, default = "lab", help="lab name to put in protein ID (default: 'lab')")
	parser.add_argument("-o", "--output", type=str, help="output file name (default : stdout)")

	args = parser.parse_args()

	if args.gff:
		file = open(args.gff, 'r')
	else:
		file = sys.stdin

	if args.output:
		output = open(args.output, 'w')
	else:
		output = sys.stdout

	fasta_file         = args.fasta
	gene_product_table = args.gene_product
	lab_name           = args.lab_name

	main(file, output, fasta_file, gene_product_table, lab_name)

	file.close()
	output.close()
